# 106bombyx

Mathematic epitech's project.

In the 70’s, chaos theory opened the way for a better understanding of the evolution of some animal
species. Butterflys for instance. Let’s look at. . . bombyx.


In order to study this evolution, you are asked to plot two things:
* The curve representing the number of individuals in relation to the generation (varying from 1 to 100);
* A synthetic scheme summing all the results for a given n; it consists in plotting every value of xi
(between two given bounds), in relation to k (k varying from 1 to 4 by 0.01 steps).

___I pass 100% of the tests, no norm errors___

## Usage
```
USAGE
    ./106bombyx n [k | i0 i1]
DESCRIPTION
    n  number of first generation individuals
    k  growth rate from 1 to 4
    i0 initial generation (included)
    i1 final generation (included)
```