#!/usr/bin/python3

def print_helper(name):
    print("USAGE")
    print("\t" + str(name) + " n [k | i0 i1]\n")
    print("DESCRIPTION")
    print("\tn\tnumber of first generation individuals")
    print("\tk\tgrowth rate from 1 to 4")
    print("\ti0\tinitial generation (included)")
    print("\ti1\tfinal generation (included)")
    exit(0)