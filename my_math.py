#!/usr/bin/python3

import math

def my_calc(n, k, i_zero = 1, i_one = 100):
    if (k < 1 or k > 4):
        print("[Error] k must be from 1 to 4")
        exit(84)
    print(str(i_zero), "%.2f" % (n))
    for i in range (i_zero, i_one):
        n = k * n * (1000 - n) / 1000
        if (n < 0):
            n = 0
        print(str(i + 1), "%.2f" % (n))
    exit(0)

def second_part(n, i_zero, i_one):
    k = 1
    if ((i_zero < 0 or i_one < 0) or i_zero > i_one):
        print("[Error] i0 and i0 must be positive number")
        exit(84)
    while k < 4:
        x = n
        for i in range(1, i_one + 1):
            if (i >= i_zero):
                print("%.2f %.2f" % (k, x))
            x = k * x * (1000 - x) / 1000
            if (x < 0):
                x = 0
        k += 0.01
    exit(0)